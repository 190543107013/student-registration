package com.example.studentregistration;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

public class TblMstCollege extends Mydatabase {

    public static final String TABLE_NAME="TblMstCollage";
    public static final String COLLEGE_ID="CollageId";
    public static final String COLLEGE_NAME="Name";
    public TblMstCollege(Context context) {
        super(context);
    }
public ArrayList<CollegeModel> getCollegeList()
{
    SQLiteDatabase db = getReadableDatabase();
    ArrayList<CollegeModel> list =new ArrayList<>();
    String qurey = "SELECT * FROM " + TABLE_NAME;
    Cursor cursor = db.rawQuery(qurey,null);
    cursor.moveToFirst();
    for (int i=0;i<cursor.getCount();i++)
    {
       CollegeModel collegeModel = new CollegeModel();
        collegeModel.setCollageId(cursor.getInt(cursor.getColumnIndex(COLLEGE_ID)));
        collegeModel.setName(cursor.getString(cursor.getColumnIndex(COLLEGE_NAME)));
        list.add(collegeModel);
        cursor.moveToNext();
    }
    cursor.close();
    db.close();
    return list;
}
}
