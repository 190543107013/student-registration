package com.example.studentregistration;

import java.io.Serializable;

public class CollegeModel implements Serializable {
    int CollageId;
    String Name;

    public int getCollageId() {
        return CollageId;
    }

    public void setCollageId(int collageId) {
        CollageId = collageId;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    @Override
    public String toString() {
        return "CollegeModel{" +
                "CollageId=" + CollageId +
                ", Name='" + Name + '\'' +
                '}';
    }

}
