package com.example.studentregistration;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

public class TblStudent extends Mydatabase {
    public static final String TABLE_NAME = "TblUser";
    public static final String USER_ID = "UserId";
    public static final String STUDENT_NAME = "Name";
    public static final String FATHER_NAME = "FatherName";
    public static final String SUR_NAME = "SurName";
    public static final String ENROLMENT_NUMBER = "EnrollmentNumber";
    public static final String DOB = "Dob";
    public static final String EMAIL = "Email";
    public static final String PHONE_NUMBER = "PhoneNumber";
    public static final String COLLEGE_ID = "CollageId";
    public static final String CITY_ID = "CityId";
    public static final String BRANCH = "Branch";
    public static final String ADDRESS = "Address";

    public static final String CITY="City";
    public static final String COLLEGE="Collage";

    public TblStudent(Context context) {
        super(context);
    }

    public StudentModel getdetails(Cursor cursor)
    {
        StudentModel model =new StudentModel();
        model.setUserId(cursor.getInt(cursor.getColumnIndex(USER_ID)));
        model.setCityId(cursor.getInt(cursor.getColumnIndex(CITY_ID)));
        model.setCollageId(cursor.getInt(cursor.getColumnIndex(COLLEGE_ID)));
        model.setEnrollmentNumber(cursor.getInt(cursor.getColumnIndex(ENROLMENT_NUMBER)));
        model.setName(cursor.getString(cursor.getColumnIndex(STUDENT_NAME)));
        model.setFatherName(cursor.getString(cursor.getColumnIndex(FATHER_NAME)));
        model.setSurName(cursor.getString(cursor.getColumnIndex(SUR_NAME)));
        model.setDob(cursor.getString(cursor.getColumnIndex(DOB)));
        model.setEmail(cursor.getString(cursor.getColumnIndex(EMAIL)));
        model.setPhoneNumber(cursor.getString(cursor.getColumnIndex(PHONE_NUMBER)));
        model.setBranch(cursor.getString(cursor.getColumnIndex(BRANCH)));
        model.setAddress(cursor.getString(cursor.getColumnIndex(ADDRESS)));
        model.setCity(cursor.getString(cursor.getColumnIndex(CITY)));
        model.setCollege(cursor.getString(cursor.getColumnIndex(COLLEGE)));
        return model;
    }

    public ArrayList<StudentModel> GetStudentList()
    {
        SQLiteDatabase db =getReadableDatabase();
        ArrayList<StudentModel> list =new ArrayList<>();
        String qurey = ( " SELECT " +
                " UserId," +
                " TblUser.Name," +
                " FatherName," +
                " SurName," +
                " EnrollmentNumber," +
                " Dob," +
                " Email," +
                " PhoneNumber,"+
                " TblMstCollage.CollageId," +
                " TblMstCity.CityId," +
                " Branch,"+
                " Address,"+

                " TblMstCollage.Name as Collage," +
                " TblmstCity.Name as City " +

                " FROM " +
                " TblUser " +
                " INNER JOIN TblMstCollage ON TblUser.CollageId = TblMstCollage.CollageId " +
                " INNER JOIN TblmstCity ON TblUser.CityId = TblmstCity.CityId ");
        Cursor cursor =db.rawQuery(qurey,null);
        cursor.moveToFirst();
        for(int i=0 ; i < cursor.getCount(); i++)
        {
            list.add(getdetails(cursor));
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
        return list;
    }

    public ArrayList<StudentModel> GetStudentListBYEnrol(int EnrollmentNumber)
    {
        SQLiteDatabase db =getReadableDatabase();
        ArrayList<StudentModel> list =new ArrayList<>();
        String qurey = ( " SELECT " +
                " UserId," +
                " TblUser.Name," +
                " FatherName," +
                " SurName," +
                " EnrollmentNumber," +
                " Dob," +
                " Email," +
                " PhoneNumber,"+
                " TblMstCollage.CollageId," +
                " TblMstCity.CityId," +
                " Branch,"+
                " Address,"+

                " TblMstCollage.Name as Collage," +
                " TblmstCity.Name as City " +

                " FROM " +
                " TblUser " +
                " INNER JOIN TblMstCollage ON TblUser.CollageId = TblMstCollage.CollageId " +
                " INNER JOIN TblmstCity ON TblUser.CityId = TblmstCity.CityId " +

                " WHERE " +
                "EnrollmentNumber = ?"
                );
        Cursor cursor =db.rawQuery(qurey,null);
        cursor.moveToFirst();
        for(int i=0 ; i < cursor.getCount(); i++)
        {
            list.add(getdetails(cursor));
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
        return list;
    }

    public StudentModel GetStudentById(int id)
    {
        SQLiteDatabase db =getReadableDatabase();
        StudentModel model =new StudentModel();
        String query = " SELECT * FROM " + TABLE_NAME + " WHERE " + USER_ID + " = ?";
        Cursor cursor = db.rawQuery(query,new String[]{String.valueOf(id)});
        cursor.moveToFirst();
        model.setUserId(cursor.getInt(cursor.getColumnIndex(USER_ID)));
        model.setCityId(cursor.getInt(cursor.getColumnIndex(CITY_ID)));
        model.setCollageId(cursor.getInt(cursor.getColumnIndex(COLLEGE_ID)));
        model.setEnrollmentNumber(cursor.getInt(cursor.getColumnIndex(ENROLMENT_NUMBER)));
        model.setName(cursor.getString(cursor.getColumnIndex(STUDENT_NAME)));
        model.setFatherName(cursor.getString(cursor.getColumnIndex(FATHER_NAME)));
        model.setSurName(cursor.getString(cursor.getColumnIndex(SUR_NAME)));
        model.setDob(cursor.getString(cursor.getColumnIndex(DOB)));
        model.setEmail(cursor.getString(cursor.getColumnIndex(EMAIL)));
        model.setPhoneNumber(cursor.getString(cursor.getColumnIndex(PHONE_NUMBER)));
        model.setBranch(cursor.getString(cursor.getColumnIndex(BRANCH)));
        model.setAddress(cursor.getString(cursor.getColumnIndex(ADDRESS)));
        cursor.close();
        db.close();
        return model;
    }
    public long insertStudent(String name,String fname,
                              String sname,int enroment,
                              String dob,String email,
                              String phonenum,int cityid,
                              int collegeid,String branch,String address)
    {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(STUDENT_NAME,name);
        cv.put(FATHER_NAME,fname);
        cv.put(SUR_NAME,sname);
        cv.put(ENROLMENT_NUMBER,enroment);
        cv.put(DOB,dob);
        cv.put(EMAIL,email);
        cv.put(PHONE_NUMBER,phonenum);
        cv.put(CITY_ID,cityid);
        cv.put(COLLEGE_ID,collegeid);
        cv.put(BRANCH,branch);
        cv.put(ADDRESS,address);
        long lastid = db.insert(TABLE_NAME,null,cv);
        db.close();
        return lastid;
    }
}
