package com.example.studentregistration;

import java.io.Serializable;

public class StudentModel implements Serializable {

    int UserId;
    int EnrollmentNumber;
    int CollageId;
    int CityId;
    String Name;
    String FatherName;
    String SurName;
    String Dob;
    String Email;
    String PhoneNumber;
    String Branch;
    String Address;
    String College;
    String City;

    public String getCollege() {
        return College;
    }

    public void setCollege(String college) {
        College = college;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    @Override
    public String toString() {
        return "StudentModel{" +
                "UserId=" + UserId +
                ", EnrollmentNumber=" + EnrollmentNumber +
                ", CollageId=" + CollageId +
                ", CityId=" + CityId +
                ", Name='" + Name + '\'' +
                ", FatherName='" + FatherName + '\'' +
                ", SurName='" + SurName + '\'' +
                ", Dob='" + Dob + '\'' +
                ", Email='" + Email + '\'' +
                ", PhoneNumber='" + PhoneNumber + '\'' +
                ", Branch='" + Branch + '\'' +
                ", Address='" + Address + '\'' +
                ", College='" + College + '\'' +
                ", City='" + City + '\'' +
                '}';
    }

    public int getUserId() {
        return UserId;
    }

    public void setUserId(int userId) {
        UserId = userId;
    }

    public int getEnrollmentNumber() {
        return EnrollmentNumber;
    }

    public void setEnrollmentNumber(int enrollmentNumber) {
        EnrollmentNumber = enrollmentNumber;
    }

    public int getCollageId() {
        return CollageId;
    }

    public void setCollageId(int collageId) {
        CollageId = collageId;
    }

    public int getCityId() {
        return CityId;
    }

    public void setCityId(int cityId) {
        CityId = cityId;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getFatherName() {
        return FatherName;
    }

    public void setFatherName(String fatherName) {
        FatherName = fatherName;
    }

    public String getSurName() {
        return SurName;
    }

    public void setSurName(String surName) {
        SurName = surName;
    }

    public String getDob() {
        return Dob;
    }

    public void setDob(String dob) {
        Dob = dob;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getPhoneNumber() {
        return PhoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        PhoneNumber = phoneNumber;
    }

    public String getBranch() {
        return Branch;
    }

    public void setBranch(String branch) {
        Branch = branch;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }
}
