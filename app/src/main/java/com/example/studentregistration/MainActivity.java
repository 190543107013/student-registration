package com.example.studentregistration;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {


    @BindView(R.id.imagereg)
    ImageView imagereg;
    @BindView(R.id.cvactregs)
    CardView cvactregs;
    @BindView(R.id.imagelist)
    ImageView imagelist;
    @BindView(R.id.cvactlst)
    CardView cvactlst;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);


    }

    @OnClick(R.id.cvactregs)
    public void onCvactregsClicked() {
        Intent i = new Intent(MainActivity.this, studentreg.class);
        startActivity(i);
    }

    @OnClick(R.id.cvactlst)
    public void onCvactlstClicked() {
        Intent i = new Intent(MainActivity.this, studentlist.class);
        startActivity(i);
    }
}