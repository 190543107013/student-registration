package com.example.studentregistration;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StudentAdapter extends RecyclerView.Adapter<StudentAdapter.StudentHolder> {

    Context context;
    ArrayList<StudentModel> StudentList;
    onViewCilickListner onViewCilickListner;

    public StudentAdapter(Context context, ArrayList<StudentModel> StudentList, onViewCilickListner onViewCilickListner) {
        this.context = context;
        this.StudentList = StudentList;
        this.onViewCilickListner = onViewCilickListner;
    }
    public interface onViewCilickListner
    {
        void onclick(int position);
        void onFavroiteClick(int position);
        void onItemclick(int position);
    }
    @NonNull
    @Override
    public StudentHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        return new StudentHolder(LayoutInflater.from(context).inflate(R.layout.view_row_stud_list, null));
    }

    @Override
    public void onBindViewHolder(@NonNull StudentHolder holder, int position) {
        holder.tvfullname.setText(StudentList.get(position).Name + " "+StudentList.get(position).FatherName+" "+StudentList.get(position).SurName);
        String enrol = String.valueOf(StudentList.get(position).EnrollmentNumber);
        holder.enrno.setText(enrol);
        holder.tvDob.setText(StudentList.get(position).Dob);

        holder.itemView.setOnClickListener(view -> {
            if(onViewCilickListner != null)
            {
                onViewCilickListner.onItemclick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return StudentList.size();
    }

    class StudentHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvfullname)
        TextView tvfullname;
        @BindView(R.id.enrno)
        TextView enrno;
        @BindView(R.id.tvDob)
        TextView tvDob;
        public StudentHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


}
