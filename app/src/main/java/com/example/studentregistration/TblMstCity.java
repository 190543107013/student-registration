package com.example.studentregistration;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

public class TblMstCity extends Mydatabase {
    public static final String TABLE_NAME = "TblMstCity";
    public static final String CITY_ID = "CityId";
    public static final String CITY_NAME = "Name";

    public TblMstCity(Context context) {
        super(context);
    }
    public ArrayList<CityModel> getCityList()
    {
        SQLiteDatabase db = getReadableDatabase();
        ArrayList<CityModel> list =new ArrayList<>();
        String qurey = "SELECT * FROM " + TABLE_NAME;
        Cursor cursor = db.rawQuery(qurey,null);
        cursor.moveToFirst();
        for (int i=0;i<cursor.getCount();i++)
        {
           CityModel cityModel = new CityModel();
            cityModel.setCityId(cursor.getInt(cursor.getColumnIndex(CITY_ID)));
            cityModel.setName(cursor.getString(cursor.getColumnIndex(CITY_NAME)));
            list.add(cityModel);
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
        return list;
    }
    public CityModel getCityByid(int id)
    {
        SQLiteDatabase db = getReadableDatabase();
        CityModel model =new CityModel();
        String qurey ="SELECT * FROM "+ TABLE_NAME + " WHERE "+ CITY_ID + " = ?";
        Cursor cursor = db.rawQuery(qurey,new String[]{String.valueOf(id)});
        cursor.moveToFirst();
        model.setCityId(cursor.getInt(cursor.getColumnIndex(CITY_ID)));
        model.setName(cursor.getString(cursor.getColumnIndex(CITY_NAME)));
        cursor.close();
        db.close();
        return model;
    }
}
