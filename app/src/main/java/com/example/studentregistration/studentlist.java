package com.example.studentregistration;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class studentlist extends AppCompatActivity {

    @BindView(R.id.rcvUserlist)
    RecyclerView rcvUserlist;

    ArrayList<StudentModel> studentList = new ArrayList<>();
    StudentAdapter studentAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_list);
        ButterKnife.bind(this);
        setAdapter();
    }

    void setAdapter()
    {
     rcvUserlist.setLayoutManager(new GridLayoutManager(this,1));
     studentList.addAll(new TblStudent(this).GetStudentList());
     studentAdapter =new StudentAdapter(this,studentList, new StudentAdapter.onViewCilickListner() {
         @Override
         public void onclick(int position) {

         }

         @Override
         public void onFavroiteClick(int position) {

         }

         @Override
         public void onItemclick(int position) {
             Intent intent = new Intent(getApplicationContext(),studentreg.class);
             intent.putExtra(Constant.USER_OBJECT,studentList.get(position));
             startActivity(intent);
         }
     });
     rcvUserlist.setAdapter(studentAdapter);

    }
}
