package com.example.studentregistration;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputEditText;

import java.util.ArrayList;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;


public class studentreg extends AppCompatActivity {

    Button btn_registration;
    TextInputEditText etname, etfname, etsname, etdob, etemail, etphone, enrno, etadd;
    EditText etbranch;
    Spinner spcity, spclg;

    CityAdapter cityAdapter;
    CollegeAdapter collegeAdapter;

    ArrayList<CityModel> citylist = new ArrayList<>();
    ArrayList<CollegeModel> collegelist = new ArrayList<>();
    @BindView(R.id.Screen_Layout)
    FrameLayout ScreenLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.secondactivity);
        ButterKnife.bind(this);
        idgenrater();
        SetSpinnerAdepter();
        setdatecurrent();
        Eventhandler();
    }

    public void idgenrater() {
        etname = findViewById(R.id.etname);
        etfname = findViewById(R.id.etfname);
        etsname = findViewById(R.id.etsname);
        etdob = findViewById(R.id.etdob);
        etemail = findViewById(R.id.etemail);
        etbranch = findViewById(R.id.etbranch);
        etphone = findViewById(R.id.etphone);
        enrno = findViewById(R.id.enrno);
        etadd = findViewById(R.id.etadd);
        spcity = findViewById(R.id.spcity);
        spclg = findViewById(R.id.spclg);
        btn_registration = findViewById(R.id.login);
    }

    void SetSpinnerAdepter() {
        citylist.addAll(new TblMstCity(this).getCityList());
        collegelist.addAll(new TblMstCollege(this).getCollegeList());

        cityAdapter = new CityAdapter(this, citylist);
        collegeAdapter = new CollegeAdapter(this, collegelist);

        spcity.setAdapter(cityAdapter);
        spclg.setAdapter(collegeAdapter);


    }

    void setdatecurrent() {
        final Calendar newCalendar = Calendar.getInstance();
        etdob.setText(String.format("%02d", newCalendar.get(Calendar.DAY_OF_MONTH)) + "/" +
                String.format("%02d", newCalendar.get(Calendar.MONTH)) + "/" +
                String.format("%02d", newCalendar.get(Calendar.YEAR)));
    }

    public void Eventhandler() {
        btn_registration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isvaliduser()) {
                    long lastid = new TblStudent(getApplicationContext()).insertStudent(etname.getText().toString(), etfname.getText().toString(),
                            etsname.getText().toString(), Integer.parseInt(enrno.getText().toString()), utils.getformatedstingtoinsert(etdob.getText().toString()), etemail.getText().toString(),
                            etphone.getText().toString(), citylist.get(spcity.getSelectedItemPosition()).CityId,
                            collegelist.get(spclg.getSelectedItemPosition()).CollageId, etbranch.getText().toString(), etadd.getText().toString());
                    if (lastid > 0) {
                        Toast.makeText(getApplicationContext(), "user inserted sussfully", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getApplicationContext(), "user not inserted sussfully", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        etdob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar newCalendar = Calendar.getInstance();
                DatePickerDialog datePickerDialog = new DatePickerDialog(
                        studentreg.this, (datePicker, year, month, day) -> {
                    etdob.setText(day + "/" + (month + 1) + "/" + year);
                },
                        newCalendar.get(Calendar.YEAR),
                        newCalendar.get(Calendar.MONTH),
                        newCalendar.get(Calendar.DAY_OF_MONTH));

                datePickerDialog.show();
            }
        });

    }

    Boolean isvaliduser() {
        boolean isvalid = true;
        if (TextUtils.isEmpty(etname.getText().toString())) {
            isvalid = false;
            etname.setError("Enetr valid Name");
        }
        if (TextUtils.isEmpty(etfname.getText().toString())) {
            isvalid = false;
            etfname.setError("Enetr father Name");
        }
        if (TextUtils.isEmpty(etsname.getText().toString())) {
            isvalid = false;
            etsname.setError("Enetr sur Name");
        }
        if (TextUtils.isEmpty(etphone.getText().toString())) {
            isvalid = false;
            etphone.setError("Enetr Phone Number");
        } else if (etphone.getText().toString().length() < 10) {
            isvalid = false;
            etphone.setError("enter valid number");
        }
        if (TextUtils.isEmpty(etemail.getText().toString())) {
            isvalid = false;
            etemail.setError("Enetr Email addrs");
        } else {
            String emil = etemail.getText().toString().trim();
            String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
            if (!emil.matches(emailPattern)) {
                etemail.setError("email is not valid");
                isvalid = false;
            }
        }
        if (spcity.getSelectedItemPosition() == 0) {
            isvalid = false;
            Toast.makeText(getApplicationContext(), "Select City", Toast.LENGTH_SHORT).show();
        }
        if (spclg.getSelectedItemPosition() == 0) {
            isvalid = false;
            Toast.makeText(getApplicationContext(), "Select College", Toast.LENGTH_SHORT).show();
        }
        return isvalid;
    }

}
