package com.example.studentregistration;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CollegeAdapter extends BaseAdapter {
    Context context;
    ArrayList<CollegeModel> collegelist;

    public CollegeAdapter(Context context, ArrayList<CollegeModel> collegelist) {
        this.context = context;
        this.collegelist = collegelist;
    }

    @Override
    public int getCount() {
        return collegelist.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v = view;
        CityAdapter.ViewHolder viewHolder;
        if (v == null) {
            v = LayoutInflater.from(context).inflate(R.layout.view_row_text, null);
            viewHolder = new CityAdapter.ViewHolder(v);
            v.setTag(viewHolder);
        } else {
            viewHolder = (CityAdapter.ViewHolder) v.getTag();
        }
        viewHolder.tvname.setText(collegelist.get(i).getName());
        return v;
    }

    static
    class ViewHolder {
        @BindView(R.id.tvname)
        TextView tvname;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
