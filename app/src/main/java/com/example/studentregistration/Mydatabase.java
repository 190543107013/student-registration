package com.example.studentregistration;

import android.content.Context;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

public class Mydatabase extends SQLiteAssetHelper {
    public static final String Databse_Name = "Studentdetails.db";
    public static final int Database_Version = 1;

    public Mydatabase(Context context)
    {
        super(context,Databse_Name,null,Database_Version);
    }

}
